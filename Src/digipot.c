//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//
//  \file   
//  \author Americo Lorenzana
//  \date   
//  \brief  
//
//------------------------------------------------------------------------------

#include <string.h>
#include "Periodic.h"
#include "Pin.h"
#include "Generic.h"
#include "digipot.h"

#define UP_DOWN_CONTROL_A_PIN(x)    PinWrite(PinUDA, (bool)x)
#define UP_DOWN_CONTROL_B_PIN(x)    PinWrite(PinUDB, (bool)x)

#define SET_CHP_SEL_A_PIN        PinWrite(PinCSA, (bool)1)
#define CLR_CHP_SEL_A_PIN        PinWrite(PinCSA, (bool)0)
#define SET_CHP_SEL_B_PIN        PinWrite(PinCSA, (bool)1)
#define CLR_CHP_SEL_B_PIN        PinWrite(PinCSA, (bool)0)

#define SET_INC_CONTROL_A_PIN    PinWrite(PinINCA, (bool)1)
#define CLR_INC_CONTROL_A_PIN    PinWrite(PinINCA, (bool)0)
#define SET_INC_CONTROL_B_PIN    PinWrite(PinINCB, (bool)1)
#define CLR_INC_CONTROL_B_PIN    PinWrite(PinINCB, (bool)0)

#define INC_MAX_FREQ_HZ     ((f32)20000)

#define WIPER_IS_STOPPED    0
#define WIPER_IS_MOVING     1

#define DECREMENT_W_L       0
#define INCREMENT_W_L       1

#define REQUEST_CLEARED     0
#define REQUEST_PENDING     1

typedef struct
{
    u8 MovWiper         : 1;    // Indicates wiper is moving towards either Low terminal or High terminal.
    u8 UpDownControl    : 1;    // When 0 indicates Rwl resistance is decreasing, otherwise increasing
    u8 RequestPending   : 1;    // New requested to change Rwl resistance.  
    u8 unused1          : 1;
    u8 unused2          : 1;
    u8 unused3          : 1;
    u8 unused4          : 1;
    u8 unused5          : 1;
}flags_t;

typedef struct
{
    digipot_value_t current_wiper;
    digipot_value_t target_wiper;   
    callback_t callback;            //Callback to notify resistance commanded has been reached
    flags_t state;
}digipot_data_t;

typedef struct
{
    digipot_data_t channelA;
    digipot_data_t channelB;
}digipot_t;

typedef enum
{
    DIGIPOT_IDLE_STATE = 0,
    DIGIPOT_REQ_MOV,
    DIGIPOT_INCREMENT_CONTROL_STATE,
    DIGIPOT_IS_TARGET_REACHED,
    DIGIPOT_WIPERS_STOPPED,
} digipot_states_t;

static digipot_t digipot;
static digipot_states_t digipot_current_state;  //variable to store next state to be perform by FSM.
static void digipot_state_machine(void);

err_t digipot_module_init(void)
{
    //make sure digipot variable is cleared.
    memset((void*)digipot, 0, sizeof(digipot_t));
    //Initializing channel A
    digipot.channelA.current_wiper = DIGIPOT_WIPER_MIDSCALE;
    digipot.channelA.target_wiper = DIGIPOT_WIPER_MIDSCALE;
    //Initializing channel B
    digipot.channelB.current_wiper = DIGIPOT_WIPER_MIDSCALE;
    digipot.channelB.target_wiper = DIGIPOT_WIPER_MIDSCALE;
    //Initializing FSM
    digipot_current_state = DIGIPOT_IDLE_STATE;

    // Initialize periodic module
    PeriodicModuleInit();
    PeriodicConfig(INC_MAX_FREQ_HZ ,digipot_state_machine);
    //Initializing Pin module.
    PinModuleInit();
    SET_CHP_SEL_A_PIN;  //CS channel A High
    SET_CHP_SEL_B_PIN;  //CS channel B High

    return DIGIPOT_OK;
}

err_t digipot_set_wiper(digipot_channel_t channel, digipot_value_t *wiper,callback_t cb)
{
    //pointer to channel data.
    register digipot_data_t *data_ptr = (channel == CHANNEL_A) ? &digipot.channelA : &digipot.channelB;

    if(channel > CHANNEL_B){    //channel is invalid
        return DIGIPOT_ERR_INVALID_CHANNEL;
    }
    if(wiper == NULL){          //wiper pointer is null
        return DIGIPOT_ERR_INVALID_ARG;
    }
    //Current channel is moving or pending to move.
    if(data_ptr.state->MovWiper || data_ptr.state->RequestPending){
        return DIGIPOT_ERR_INVALID_BUSY;
    }
    //requested resistance is equal than current.
    if(data_ptr->current_wiper == *wiper){
        return DIGIPOT_ERR_INVALID_ARG;
    }

    data_ptr->target_wiper = *wiper;    // update target wiper
    
    // change state flags
    data_ptr.state->RequestPending = REQUEST_PENDING;
    //Incrementing or decrementin
    if(data_ptr->current_wiper > data_ptr->target_wiper){
        data_ptr.state->UpDownControl = DECREMENT_W_L;
    }else{
        data_ptr.state->UpDownControl = INCREMENT_W_L;
    }

    //In case that cb is equal to NULL means that callback feature is not enabled.
    if(cb != NULL){
        data_ptr->callback = cb; //
    }

    //Is there any digi pot channel on the move?
    if(!digipot.channelA.state.MovWiper || !digipot.channelB.state.MovWiper){
        //No, let us trigger periodic timer to activate state machine.
        PeriodicIruptFlagClear();
        PeriodicIruptEnable();
        PeriodicStart();
    }else{
        //Yes, state machine is already active.
        //do nothing.
    }


    return DIGIPOT_OK;
}

err_t digipot_get_wiper(digipot_channel_t channel, digipot_value_t *wiper)
{
    if(channel > CHANNEL_B){    //channel is invalid
        return DIGIPOT_ERR_INVALID_CHANNEL;
    }
    if(wiper == NULL){          //wiper pointer is null
        return DIGIPOT_ERR_INVALID_ARG;
    }

    *wiper = (channel == CHANNEL_A) ? digipot.channelA.current_wiper : digipot.channelB.current_wiper;
    return DIGIPOT_OK;
}

err_t digipot_force_stop_wiper(digipot_channel_t channel)
{
    register digipot_data_t *data_ptr;

    if(channel > CHANNEL_B){    //channel is invalid
        return DIGIPOT_ERR_INVALID_CHANNEL;
    }

    data_ptr = (channel == CHANNEL_A) ? &digipot.channelA : &digipot.channelB;
    data_ptr->target_wiper = data_ptr->current_wiper;    // update target wiper

    // change state flags
    data_ptr.state->MovWiper = WIPER_IS_STOPPED;
    
    return DIGIPOT_OK;
}

bool digipot_is_moving(digipot_channel_t channel)
{
    // FIXME: assert channel
    /**
    if(channel > CHANNEL_B){
        return DIGIPOT_ERR_INVALID_CHANNEL;
    }
    */
    // is dig-pot moving?
    return( channel == CHANNEL_A ? digipot.channelA.state.MovWiper : digipot.channelBstate.MovWiper );
}


/**
 * @brief   State machine that implements basic functionality to control digital potentiometer
 *          by controlling CS, UD and INC pins. State machine will run until resistance commanded is achieved.
 *          Single state machine to control dual digital pot by asserting separately control pins for each channel, 
 *          when wiper is moving to new resistance new requests are not served, unless force stop.
 */
static void digipot_state_machine(void)
{
    //clear interrupt flag
    PeriodicIruptFlagClear();

    switch(digipot_current_state)
    {
        case DIGIPOT_IDLE_STATE:
            digipot_current_state = DIGIPOT_REQ_MOV;
        break;

        case DIGIPOT_REQ_MOV:
            digipot_current_state = DIGIPOT_WIPERS_STOPPED;
            if(digipot.channelA.state.RequestPending){
                digipot.channelA.state.RequestPending = REQUEST_CLEARED;
                digipot.channelA.state.MovWiper = WIPER_IS_MOVING;
                CLR_CHP_SEL_A_PIN;      //CS active (low)
                SET_INC_CONTROL_A_PIN;  //INC high

                // up/down control channel A
                UP_DOWN_CONTROL_A_PIN(digipot.channelA.state.UpDownControl);
                digipot_current_state = DIGIPOT_INCREMENT_CONTROL_STATE;
            }
            if(digipot.channelB.state.RequestPending){
                digipot.channelB.state.RequestPending = REQUEST_CLEARED;
                digipot.channelB.state.MovWiper = WIPER_IS_MOVING;
                CLR_CHP_SEL_B_PIN;      //CS active (low)
                SET_INC_CONTROL_B_PIN;  //INC high
                // up/down control channel B
                UP_DOWN_CONTROL_B_PIN(digipot.channelB.state.UpDownControl);
                digipot_current_state = DIGIPOT_INCREMENT_CONTROL_STATE;
            }

        break;

        case DIGIPOT_INCREMENT_CONTROL_STATE;
            digipot_current_state = DIGIPOT_IS_TARGET_REACHED;
            // Falling edge channel A
            if(digipot.channelA.current_wiper != digipot.channelA.target_wiper){
                CLR_INC_CONTROL_A_PIN; // Falling edge.
                //Increment/Decrement current wiper register depending on UpDownControl flag.
                digipot.channelA.state.UpDownControl == INCREMENT_W_L ? digipot.channelA.current_wiper++ : digipot.channelA.current_wiper--;
            }

            // Falling edge channel B
            if(digipot.channelB.current_wiper != digipot.channelB.target_wiper){
                CLR_INC_CONTROL_B_PIN; // Falling edge.
                //Increment/Decrement current wiper register depending on UpDownControl flag.
                digipot.channelB.state.UpDownControl == INCREMENT_W_L ? digipot.channelB.current_wiper++ : digipot.channelB.current_wiper--;
            }
        break;
        
        case DIGIPOT_IS_TARGET_REACHED:
            //Prepare INC control for next falling edge.
            SET_INC_CONTROL_A_PIN;
            SET_INC_CONTROL_B_PIN;
            // Target reached channel A
            if (digipot.channelA.current_wiper == digipot.channelA.target_wiper){
                digipot.channelA.state.MovWiper = WIPER_IS_STOPPED; //Marked as stopped.
                SET_CHP_SEL_A_PIN;  //CS disable (high)
                //resistance has been reached successfully.
                if(digipot.channelA.callback != NULL) digipot.channelA.callback();
                digipot.channelA.callback() = NULL; // notification callback
            }
            // Target reached channel B 
            if (digipot.channelB.current_wiper == digipot.channelB.target_wiper){}
                digipot.channelB.state.MovWiper = WIPER_IS_STOPPED; //Marked as stopped.
                SET_CHP_SEL_B_PIN;  //CS disable (high)
                //resistance has been reached successfully.
                if(digipot.channelB.callback != NULL) digipot.channelB.callback();
                digipot.channelB.callback() = NULL; // notification callback
            }

            //Is there any digi pot channel on the move?
            if(digipot.channelA.state.MovWiper || digipot.channelB.state.MovWiper){
                digipot_current_state = DIGIPOT_INCREMENT_CONTROL_STATE;    // keep issue INC control falling edge
            }else{
                digipot_current_state = DIGIPOT_WIPERS_STOPPED; // stop state machine
            }
            // Is there any new request to change resistance?
            if(digipot.channelA.state.RequestPending || digipot.channelB.state.RequestPending){
                digipot_current_state = DIGIPOT_REQ_MOV;
            }            
        break;

            /**
             * Stop state machine, stop periodic timer and to idle state to make it ready for next request.
             */
        case DIGIPOT_WIPERS_STOPPED:
        default:
            PeriodicIruptDisable();
            PeriodicStop();
            PeriodicIruptFlagClear();
            digipot_current_state = DIGIPOT_IDLE_STATE;
        break;
    }
}