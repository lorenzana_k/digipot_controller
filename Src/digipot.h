#ifndef DIGIPOT_H_
#define DIGIPOT_H_

#include "Generic.h"

typedef int32_t err_t;
typedef u8 digipot_value_t;
typedef void    (*callback_t)(void);  //callback type

/**
 * Macros to convert resistance value to wiper 0-255 register value and viceversa.
 */
#define END_TO_END_RESISTANCE_VALUE	(f32)10000.0
#define RESISTANCE_TO_WIPER(x)		(u8)(((f32)x*(f32)255.0) / (END_TO_END_RESISTANCE_VALUE))
#define WIPER_TO_RESISTANCE(x)      (f32)( ((u8)x*END_TO_END_RESISTANCE_VALUE)/(f32)255.0 )

/* Definitions for error constants. */
#define DIGIPOT_OK                       0  /*!< err_t value indicating success (no error) */
#define DIGIPOT_FAIL                    -1  /*!< Generic err_t code indicating failure */

#define DIGIPOT_ERR_INVALID_CHANNEL		-2	/*!< Invalid charnnel */
#define DIGIPOT_ERR_INVALID_ARG         -3  /*!< Invalid argument */
#define DIGIPOT_ERR_INVALID_BUSY        -4  /*!< Digipot channel is busy */


#define DIGIPOT_WIPER_MIDSCALE          (digipot_value_t)128

/**
 * Channels available
 */
typedef enum
{
    CHANNEL_A = 0,
    CHANNEL_B
} digipot_channel_t;

/**
 * @brief digipot initializetion
 *
 *        Configure internal variables, state machine, 
 *        pin mdoule and periodi module.
 *
 * @param  none
 *
 * @return
 *     - DIGIPOT_OK success
 *
 */
err_t digipot_module_init(void);

/**
 * @brief set wiper register.
 *
 * @param  	Channel 	digital pot channel
 * 			wiper 		pointer to new value to set potenciomter, range 0-255
 * 			cb 			pointer to callback to notify when channel 
 * 						resistance has achieved new valued requested
 *
 * @return
 *     - DIGIPOT_OK success
 *     - DIGIPOT_ERR_INVALID_CHANNEL 	Invalid channel error
 *     - DIGIPOT_ERR_INVALID_ARG		Argument error	
 *     - DIGIPOT_ERR_INVALID_BUSY		Channel is busy.
 *
 */
err_t digipot_set_wiper(digipot_channel_t channel, digipot_value_t *wiper,callback_t cb);

/**
 * @brief get current wiper register.     
 *
 * @param  	Channel 	digital pot channel
 * 			wiper 		pointer to destination where current wiper value is to be copied.
 *
 * @return
 *     - DIGIPOT_OK success
 *     - DIGIPOT_ERR_INVALID_CHANNEL 	Invalid channel error
 *     - DIGIPOT_ERR_INVALID_ARG		Argument error	
 *
 */
err_t digipot_get_wiper(digipot_channel_t channel, digipot_value_t* wiper);

/**
 * @brief force stop digi pot channel, leaving it at its current value.
 *
 * @param  	Channel 	digital pot channel
 *
 * @return
 *     - DIGIPOT_OK success
 *     - DIGIPOT_ERR_INVALID_CHANNEL 	Invalid channel error
 *
 */
err_t digipot_force_stop_wiper(digipot_channel_t channel);

/**
 * @brief 	fetch channel current moving state
 *
 * @param 	Channel 	digital pot channel 
 *
 * @return
 *     - TRUE digi pot channel requested is moving
 *     - FALSE digi pot channel requested is stopped
 *
 */
bool digipot_is_moving(digipot_channel_t channel);



#endif /*DIGIPOT_H_*/