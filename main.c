#include <stdio.h>
#include "main.h"
#include "Generic.h"
#include "digi.pot.h"

u8 ChannelA_value;
u8 ChannelB_value;

void channelA_done(void);
void channelB_done(void);

err_t err;

void main (void)
{
	//init digipot module
	if(digipot_module_init() != DIGIPOT_OK ){
		printf("module init error\n");
		return;
	}
	/*********************************************************************
	 * Get current wiper internal register from both channels, translate it
	 * to resisante and print it.
	 *********************************************************************/
	// get current wiper register for each channel.
	if(digipot_get_wiper(CHANNEL_A, &ChannelA_value) != DIGIPOT_OK ){
		printf("Error of some sort\n");
	}
	if(digipot_get_wiper(CHANNEL_B, &ChannelB_value) != DIGIPOT_OK ){
		printf("Error of some sort\n");
	}
	printf("Wiper value ChannelA: 0x%2X ChannelB: 0x%2X\n",ChannelA_value,ChannelB_value);
	printf("Resistance ChannelA->%0.2f ChannelB->%0.2f\n", WIPER_TO_RESISTANCE(ChannelA_value), WIPER_TO_RESISTANCE(ChannelB_value));

	/*********************************************************************
	 * Set wiper register channel A to 10Kohms and Channelb to 0.0 Oohms
	 * wait until value has been achieved.
	 *********************************************************************/
	// Set channelA to 10K Ohms and ChannelB to 0.0 Ohms
	ChannelA_value = RESISTANCE_TO_WIPER(END_TO_END_RESISTANCE_VALUE);
	ChannelB_value = RESISTANCE_TO_WIPER(0.0);
	// no use of callback notification.
	if(digipot_set_wiper(CHANNEL_A, &ChannelA_value, NULL){
		printf("Error of some sort\n");
	}
	if(digipot_set_wiper(CHANNEL_B, &ChannelB_value, NULL){
		printf("Error of some sort\n");
	}

	//wait until resistance is achieved.
	while(digipot_is_moving(CHANNEL_A) || digipot_is_moving(CHANNEL_B));
	printf("resistance achieved.\n");

	/*********************************************************************
	 * Set wiper register channel A to 0.0ohms and Channel b to 10Kohms
	 * and use callback funtion to notify when values has been achieved.
	 *********************************************************************/
	// Set channelA to 0.0 Ohms and ChannelB to 10K Ohms
	ChannelA_value = RESISTANCE_TO_WIPER(0.0);
	ChannelB_value = RESISTANCE_TO_WIPER(END_TO_END_RESISTANCE_VALUE);
	// no use of callback notification.
	if(digipot_set_wiper(CHANNEL_A, &ChannelA_value, channelA_done){
		printf("Error of some sort\n");
	}
	if(digipot_set_wiper(CHANNEL_B, &ChannelB_value, channelB_done){
		printf("Error of some sort\n");
	}

	for(;;);

}

void channelA_done(void){
	printf("channelA has reached resitance commaned.\n");
}
void channelB_done(void){
	printf("channelB has reached resitance commaned.\n");
}